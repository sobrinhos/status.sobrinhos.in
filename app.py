from flask import Flask
from flask import request

import arrow
app = Flask(__name__)


@app.route("/")
def status():
    now = arrow.now().timestamp
    with open('requests.txt', 'r') as requests:
        reqs = requests.readlines()
        if reqs:
            last_request = int(reqs[-1])
            diff = now - last_request
            if diff < 300:
                return "online"

    return "offline"


@app.route('/im-alive')
def im_alive():
    pwd = request.args.get('pwd')
    if pwd == 'judith':
        with open('requests.txt', 'a') as requests:
            requests.write(str(arrow.now().timestamp) + '\n')
        return 'Massa!', 201
    return '', 400


if __name__ == "__main__":
    app.run(debug=True)
